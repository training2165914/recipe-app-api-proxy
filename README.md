# Reciepe App API proxy
NGINX proxy app or our recipe app API

## Usage

### Envrionment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

